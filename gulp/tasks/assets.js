var gulp  = require('gulp');

// copy assets
gulp.task('assets', function() {
  return gulp.src('./src/**/*.{png,jpg,eot,svg,ttf,woff,woff2,ico,txt,html}')
    .pipe(gulp.dest('./app'));
});
