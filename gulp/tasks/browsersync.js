var gulp        = require('gulp');
var browserSync = require('browser-sync');

// Runs browsersync server on port 3000
gulp.task('browser-sync', function() {
  return browserSync({
    open: true,
    server: {
        baseDir: './app/'
    },
    snippetOptions: {
      rule: {
        match: /<\/body>/i,
        fn: function(snippet, match) { return snippet + match; }
      }
    }
  });
});

// Reload all Browsers
gulp.task('bs-reload', function() {
  // if (!options['auto-reload']) return;
    return browserSync.reload();
});
