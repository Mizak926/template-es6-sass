var gulp = require('gulp');
var clean  = require('gulp-clean');

// cleanup tmp files
gulp.task('clean', function(cb) {
  return gulp.src('./app', {read: false})
    .pipe(clean());
});
