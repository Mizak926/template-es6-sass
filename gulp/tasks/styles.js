var gulp         = require('gulp');
var gutil        = require('gulp-util');
var sass         = require('gulp-sass');
var plumber      = require('gulp-plumber');
var autoprefixer = require('autoprefixer');
var postcss      = require('gulp-postcss');
var postURL      = require('postcss-url');
var url          = require('url');
var reload       = require('browser-sync').reload;

// compile all sass files
gulp.task('styles', function() {
  return gulp.src('./src/styles/index.scss')
    .pipe(plumber({ errorHandler: onError}))
    .pipe(sass())
    .pipe(postcss([
      autoprefixer({ browsers: ['last 4 version', 'Firefox 20'] })
    ]))
    .pipe(plumber.stop())
    .pipe(gulp.dest('./app'))
    .pipe(reload({ stream: true })); // auto-inject into browsers
});

// When an error occurs, display in red and beep
function onError(err) {
  gutil.beep();
  console.log(gutil.colors.red(err.message));
  this.emit('end');
}