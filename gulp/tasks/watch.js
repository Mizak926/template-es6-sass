var gulp        = require('gulp');
var paths       = require('../config');
var runSequence = require('run-sequence');

gulp.task('watch', function(callback) {
    gulp.watch('./src/**/*.{png,jpg,eot,svg,ttf,woff,woff2,ico,txt,html}', function() {
      runSequence(['assets'], 'bs-reload');
    });

    gulp.watch('./src/styles/**/*.scss', function() {
      runSequence(['styles'], 'bs-reload');
    });
});
