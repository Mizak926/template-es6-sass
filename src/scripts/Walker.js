class Walker {
  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }
  test() {
    console.log('hello walker: ', this.x, this.y);
  }
}

export default Walker;
